<?php
/**
 * @file
 *   Admin functionality for ARC module.
 */

/**
 * Builds the admin form for the ARC module.
 */
function arc_admin() {
  global $base_url;

  $form = array();

  $form['arc_base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Base URL'),
    '#default_value' => variable_get('arc_base_url', $base_url),
    '#description' => t("The base URL to test against, prepended to Drupal paths, redirects and aliases when they are validated. Defaults to the location of the current Drupal installation."),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Validation callback for the admin form.
 */
function arc_admin_validate($form, &$form_state) {
  $arc_base_url = $form_state['values']['arc_base_url'];
  if (!valid_url($arc_base_url, TRUE)) {
    form_set_error('arc_base_url', t('You must enter a valid absolute URL to check against.'));
  }
}
